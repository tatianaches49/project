using System;
using System.Diagnostics;

namespace AnalysTime
{
    internal class TimingAlg
    {
        TimeSpan duration; //хранение результата измерения
        TimeSpan[] threads; // значения времени для всех потоков процесса
        public TimingAlg()
        {
            duration = new TimeSpan(0);
            threads = new TimeSpan[Process.GetCurrentProcess().Threads.Count];
            
        }

        internal void StopTime()
        {
            TimeSpan tmp;
            for (int i = 0; i < threads.Length; i++)
            {
                tmp = Process.GetCurrentProcess().Threads[i].
                UserProcessorTime.Subtract(threads[i]); // исключение(вычитание)
                if (tmp > TimeSpan.Zero)
                  duration = tmp;
            }
        }

        internal object Result()
        {
            return duration;
        }

        internal void StartTime()
        {
            GC.Collect(); // сборка мусора
            GC.WaitForPendingFinalizers();//выполняет все необходимые завершающие действия
            for (int i = 0; i < threads.Length; i++)
                threads[i] = Process.GetCurrentProcess().Threads[i].UserProcessorTime;
        }
    }
}
