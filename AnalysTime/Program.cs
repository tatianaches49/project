using System;
using System.Diagnostics;
using System.Collections;
namespace AnalysTime
{
    class Program
    {
        delegate int Operation(int[] a); // делегат
        static int n = 30000; //размерность
        static Random rnd = new Random(); // числа для заполнения
        static void Main(string[] args)
        {

            int[] a = new int[n]; // создание массива
            for (int i = 0; i < n; i++) // цикл для прохода по массиву
                a[i] = rnd.Next(); // присвоение значений

            // Делегаты как параметры методов
            DoOperation(a, linePoisk);         
            DoOperation(a, BinaryFind);
            DoOperation(a, SortInsertion);
            DoOperation(a, Reseach);

            void DoOperation(int[] a, Operation op)
            {
                TimingAlg objT = new TimingAlg();
                Stopwatch stpWatch = new Stopwatch();
                objT.StartTime();
                stpWatch.Start();
                op(a);
                stpWatch.Stop();
                objT.StopTime();
                Console.WriteLine("StopWatch line: " +  stpWatch.Elapsed.ToString());
                Console.WriteLine("Timing line: " + objT.Result().ToString());
            }
        }

        
        private static int Reseach(int[] a) // поиск элемента
        {

            Hashtable hash = new Hashtable(n); // создание таблицы
            for (int i = 0; i < n; i++)// цикл для прохода по таблице
                hash.Add(rnd.Next(), a[i]); // присвоение значений
           
            if (hash.ContainsKey(889401235)) { // проверка содержится ли элемент в таблице
               return 1; // true
            }
            else {
                return 0; //false
            }
      
        }

        private static int linePoisk(int[] a) // линейный поиск
        {
            int b = 1485389239; // искомое значение
            int k = -1; // значение в случае отсутвия искомого
            
            for (int i = 0; i < a.Length; i++) // проход
            {
               if (a[i] == b) { k = i; } //проверка
            }
            return k;
        }
        private static int BinaryFind(int[] a) // бинарный поиск
        {
            Array.Sort(a); // массив должен быть отсортирован
            int k;   // индекс искомого элемента
            int L = 0; // левая граница
            int b = 1485389239; // искомый элемент 
            int R = a.Length - 1;  // правая граница 
            k = (R + L) / 2; // середина
           
            while (L < R - 1)
            {
               k = (R + L) / 2;
                if (a[k] == b)
                    return k; 
                if (a[k] < b)
                    L = k;
                else
                    R = k;
            }
            if (a[k] != b)
            {
                if (a[L] == b)
                    k = L;
                else
                {
                    if (a[R] == b)
                        k = R;
                    else
                        k = -1;
                };
            }
            return k;
        }


        private static int SortInsertion(int[] a) // сортировка массива
        {
            for (int i = 1; i < a.Length; i++)
            {
                int k = a[i];
                int j = i - 1;

                while (j >= 0 && a[j] > k)
                {
                    a[j + 1] = a[j];
                    a[j] = k;
                    j--;
                }
            }
            return 1;
        }

    }
}

